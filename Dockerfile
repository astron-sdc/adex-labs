FROM node:lts as BUILD_STAGE

#RUN apt-get update && apt-get install -y git bash nano mc

WORKDIR /app
COPY package*.json ./
RUN npm install

COPY . .
ENV NODE_OPTIONS="--openssl-legacy-provider"
RUN npm run build

FROM nginx:latest
RUN apt-get update && apt-get install -y bash nano mc
COPY --from=BUILD_STAGE /app/build /usr/share/nginx/html/adex-labs/
COPY conf.d/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80

# sudo docker build -t adex-labs:latest .
# sudo docker run -d --name adex-labs -p 80:80 --restart always adex-labs:latest
# docker exec -it portfolio bash