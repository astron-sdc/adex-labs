# ADEX-labs

An ADEX-like GUI to experiment, test and demonstrate the technology.

See it live: 

### Development
* http://localhost:3000/adex-labs/
* http://localhost/adex-labs/

### Test environment
* sdc-dev: https://sdc-dev.astron.nl/adex-labs/
* SURF   : http://145.38.187.31/adex-labs/

### Production environment
* sdc : https://sdc.astron.nl/adex-labs/

### run locally
```
npm install
npm run start
```

![](https://git.astron.nl/astron-sdc/adex-labs/-/blob/main/docs/adex-labs.jpg)
