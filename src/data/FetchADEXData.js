import React, {useState, useEffect} from 'react';
import {useGlobalReducer} from '../contexts/GlobalContext';
import {
    SET_STATUS_ADEX,
    SET_FETCHED_ADEX,
    SET_NUMBER_OF_DATAPRODUCTS,
    ALADIN_RELOAD,
    SET_SELECTED_PRIMARY,
    SET_STATUS_PRIMARY, SET_STATUS_ACTIVITY, SET_SELECTED_ACTIVITY
} from '../contexts/GlobalStateReducer';


export default function FetchADEXData(skipAbortController) {

    // use global state
    const [my_state, my_dispatch] = useGlobalReducer()
    const controller = new AbortController();


    useEffect(() => {
            fetchSkyviewDataAsync()

            // the initial fetch should not be aborted (when called with FetchADEXData(true))
            if (!skipAbortController) {
                return () => {
                    controller.abort();
                };
            }
        }, [my_state.reload_adex]
    );

    // fetch a primary dataproduct when the primary_dp_url has changed
    useEffect(() => {
            if (my_state.primary_dp_url) {
                fetchPrimaryDP(my_state.primary_dp_url)
            }
        }, [my_state.primary_dp_url]
    );

    // fetch an activity when the activity_url has changed
    useEffect(() => {
            if (my_state.activity_url) {
                fetchActivity(my_state.activity_url)
            }
        }, [my_state.activity_url]
    );

    // translate the state to different url parameters, depending on the type of backend (fastapi, drf)
    function constructUrl()  {

        // common values for all backend types
        let url = my_state.adex_backend.url
        let d = (my_state.adex_fov / 2)
        let ra_min = Number(my_state.adex_ra) - d
        let ra_max = Number(my_state.adex_ra) + d
        let dec_min = Number(my_state.adex_dec) - d
        let dec_max = Number(my_state.adex_dec) + d

        // for benchmarking tests (not production)
        if (my_state.adex_backend.type === 'drf') {
            url += "/adex_backend/api/v1/skyview/"
            url += "?ra__gte=" + ra_min.toString() + "&ra__lte=" + ra_max.toString()
            url += "&dec__gte=" + dec_min.toString() + "&dec__lte=" + dec_max.toString()
            url += "&collection__in=" + my_state.collection.name
            //url += "&dataproduct_type=" + my_state.dataproduct_type
            url += "&dataproduct_subtype=" + my_state.dataproduct_subtype
            url += "&page_size=" + my_state.data_limit.toString()
        } else

        // for benchmarking tests (not production)
        if (my_state.adex_backend.type === 'fastapi') {
            url += "/adex-fastapi/skyviews_dataproducts/"
            url += "?ra_min=" + ra_min.toString() + "&ra_max=" + ra_max.toString()
            url += "&dec_min=" + dec_min.toString() + "&dec_max=" + dec_max.toString()
            url += "&collections=" + my_state.collection.name
            url += "&dp_types=" + my_state.dataproduct_type
            url += "&dp_subtypes=" + my_state.dataproduct_subtype
            //url += "&levels=" + my_state.level
            url += "&limit=" + my_state.data_limit.toString()
        }

        // production endpoint
        if (my_state.adex_backend.type === 'primary_dp') {
            url += "/adex_backend/api/v1/primary_dp/?"
            //url += "&ra__gte=" + ra_min.toString() + "&ra__lte=" + ra_max.toString()
            //url += "&dec__gte=" + dec_min.toString() + "&dec__lte=" + dec_max.toString()

            // switch to cone search
            url += "&ra=" + my_state.adex_ra.toString()
            url += "&dec=" + my_state.adex_dec.toString()
            url += "&fov=" + my_state.adex_fov.toString()
            url += "&collection=" + my_state.collection.name
            url += "&dp_type=" + my_state.dataproduct_type
            url += "&page_size=" + my_state.data_limit.toString()

            // currently there is an experimental option to switch between the new cone search and the old rectangle search
            if (my_state.cone_search) {
                url += "&cone=true"
            }

            // a way to aggregate large datasets
            if (my_state.collection.distinct_field) {
                url += "&distinct_field=" + my_state.collection.distinct_field
            }
        }
        return url
    }

    function constructResults(data) {
        if (my_state.adex_backend.type === 'drf') {
            return data.results
        } else

        if (my_state.adex_backend.type === 'fastapi') {
            return data
        }

        if (my_state.adex_backend.type === 'primary_dp') {
            return data.results
        }
    }

    async function fetchSkyviewDataAsync()  {

        // only fetch when there is a collection selected to fetch
        // (this prevents a fetch during initialisation of the app).
        if (my_state.collection === undefined) {
            return null
        }

        let url = constructUrl()

        if (my_state.status_adex !== 'fetching') {
            my_dispatch({type: SET_STATUS_ADEX, status_adex: 'fetching'})
            let startTime = new Date()

            try {
                const response = await fetch(url, {signal: controller.signal})
                const data = await response.json()

                let results = constructResults(data)
                let endTime = new Date()
                let timeDiff = endTime - startTime

                my_dispatch({type: SET_FETCHED_ADEX, fetched_adex: results})
                my_dispatch({type: SET_NUMBER_OF_DATAPRODUCTS, number_of_dataproducts: results.length})
                my_dispatch({type: SET_STATUS_ADEX, status_adex: 'fetched (' + timeDiff.toString() + ' ms)'})
                my_dispatch({type: ALADIN_RELOAD, aladin_reload: !my_state.aladin_reload})

            } catch (error) {
                if (error.name === 'AbortError') {
                    console.log('Fetch was aborted');
                    my_dispatch({type: SET_NUMBER_OF_DATAPRODUCTS, number_of_dataproducts: 0})
                    my_dispatch({type: SET_STATUS_ADEX, status_adex: 'fetch aborted'})
                } else {
                    my_dispatch({type: SET_NUMBER_OF_DATAPRODUCTS, number_of_dataproducts: 0})
                    my_dispatch({type: SET_STATUS_ADEX, status_adex: 'failed'})
                }
            }
        }
    }

    // fetch a single primary dataproduct
    const fetchPrimaryDP = (url) => {
        if (my_state.status_primary !== 'fetching') {

            my_dispatch({type: SET_STATUS_PRIMARY, status_primary: "fetching"})
            fetch(url)
                .then(results => {
                    return results.json();
                })

                .then(data => {
                    let results = data.results
                    my_dispatch({type: SET_SELECTED_PRIMARY, selected_primary: results})
                    my_dispatch({type: SET_STATUS_PRIMARY, status_primary: "fetched"})
                })

                .catch(function () {
                    alert("fetch to " + url + " failed.");
                    my_dispatch({type: SET_STATUS_PRIMARY, status_primary: "error"})
                })
        }
    }

    // fetch a single activity (for the process view)
    const fetchActivity = (url) => {

        if (my_state.status_activity !== 'fetching') {

            my_dispatch({type: SET_STATUS_ACTIVITY, status_activity: "fetching"})

            fetch(url)
                .then(results => {
                    return results.json();
                })

                .then(results => {
                    my_dispatch({type: SET_SELECTED_ACTIVITY, selected_activity: results})
                    my_dispatch({type: SET_STATUS_ACTIVITY, status_activity: "fetched"})
                })

                .catch(function () {
                    alert("fetch to " + url + " failed.");
                    my_dispatch({type: SET_STATUS_ACTIVITY, status_activity: "error"})
                })
        }
    }
}

