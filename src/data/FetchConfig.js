import React, {useState, useEffect} from 'react';
import {useGlobalReducer} from '../contexts/GlobalContext';
import {
    SET_BACKEND_CONFIG, SET_COLLECTION, SET_DATAPRODUCT_TYPE, SET_SELECTED_SURVEY
} from '../contexts/GlobalStateReducer';


export default function FetchConfig() {

    // use global state
    const [my_state, my_dispatch] = useGlobalReducer()

    useEffect(() => {
            if (my_state.adex_backend.url) {
                fetchConfig(my_state.adex_backend.url)
            }
        }, [my_state.adex_backend.url,my_state.config_name]
    );

    const fetchConfig = () => {
        let url = my_state.adex_backend.url + "/adex_backend/configuration/?config_name=" + my_state.config_name

        fetch(url)
            .then(results => {
                return results.json();
            })
            .then(configuration => {
                my_dispatch({type: SET_BACKEND_CONFIG, backend_config: configuration.configuration})
                // get the default if empty

                // set some defaults
                let dp_type = configuration.configuration.config.focus.defaults.dataproduct_type
                my_dispatch({type: SET_DATAPRODUCT_TYPE, dataproduct_type: dp_type})

                let collection = configuration.configuration.config.focus.defaults.collection
                my_dispatch({type: SET_COLLECTION, collection: collection})

                let selected_survey = configuration.configuration.config.focus.defaults.selected_survey
                my_dispatch({type: SET_SELECTED_SURVEY, selected_survey: selected_survey})

            })
            .catch(function () {
                //alert("fetch to " + url + " failed.");
                my_dispatch({type: SET_BACKEND_CONFIG, backend_config: undefined})
            })
    }

}

