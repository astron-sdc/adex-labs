
// possible actions
export const SET_CONFIG_NAME = 'SET_CONFIG_NAME'
export const SET_BACKEND_CONFIG = 'SET_BACKEND_CONFIG'
export const RELOAD_CONFIG = 'RELOAD_CONFIG'
export const SET_STATUS_ADEX = 'SET_STATUS_ADEX'
export const SET_FETCHED_ADEX = 'SET_FETCHED_ADEX'
export const SET_NUMBER_OF_DATAPRODUCTS = 'SET_NUMBER_OF_DATAPRODUCTS'
export const SET_ADEX_BACKEND = 'SET_ADEX_BACKEND'
export const RELOAD_ADEX = 'RELOAD_ADEX'
export const ADEX_RA = 'ADEX_RA'
export const ADEX_DEC = 'ADEX_DEC'
export const ADEX_FOV = 'ADEX_FOV'
export const SET_LEVEL = 'SET_LEVEL'
export const SET_COLLECTION = 'SET_COLLECTION'
export const SET_DATAPRODUCT_TYPE = 'SET_DATAPRODUCT_TYPE'
export const SET_DATAPRODUCT_SUBTYPE = 'SET_DATAPRODUCT_SUBTYPE'

export const ALADIN_RELOAD = 'ALADIN_RELOAD'
export const SET_SELECTED_SURVEY = 'SET_SELECTED_SURVEY'
export const SET_VIEWED_OBJECT = 'SET_VIEWED_OBJECT'
export const SET_SELECTED_OBJECT = 'SET_SELECTED_OBJECT'
export const SET_SELECTED_PRIMARY = 'SET_SELECTED_PRIMARY'
export const SET_STATUS_PRIMARY = 'SET_STATUS_PRIMARY'
export const SET_PRIMARY_DP_URL = 'SET_PRIMARY_DP_URL'
export const SET_SELECTED_ACTIVITY = 'SET_SELECTED_ACTIVITY'
export const SET_STATUS_ACTIVITY = 'SET_STATUS_ACTIVITY'
export const SET_ACTIVITY_URL = 'SET_ACTIVITY_URL'
export const ALADIN_SET_MOUSE = 'ALADIN_SET_MOUSE'
export const SET_DATA_LIMIT = 'SET_DATA_LIMIT'
export const SET_ANCILLARY_DATA = 'SET_ANCILLARY_DATA'
export const SET_SIBLING_DATA = 'SET_SIBLING_DATA'
export const CONE_SEARCH = 'CONE_SEARCH'
export const SHOW_LABELS = 'SHOW_LABELS'
export const SET_LABEL_FIELD = 'SET_LABEL_FIELD'
export const SET_SIMBAD_ENABLED = 'SET_SIMBAD_ENABLED'

export const SET_ALTA_BACKEND = 'SET_ALTA_BACKEND'
export const RELOAD_ALTA = 'RELOAD_ALTA'
export const SET_ALTA_DATA = 'SET_ALTA_DATA'
export const SET_ALTA_COUNT = 'SET_ALTA_COUNT'
export const SET_STATUS_ALTA = 'SET_STATUS_ALTA'

export const SET_STATUS_SW = 'SET_STATUS_SW'
export const SET_SW_DATA = 'SET_SW_DATA'
export const SET_SW_COUNT = 'SET_SW_COUNT'
export const SET_SW_DATE = 'SET_SW_DATE'
export const SET_SW_STARTTIME = 'SET_SW_STARTTIME'
export const SET_SW_ENDTIME = 'SET_SW_ENDTIME'
export const RELOAD_SW = 'RELOAD_SW'

const default_adex_backend =
    process.env.NODE_ENV === "development"
        ? {
            "name": "localhost:8000",
            "type": "primary_dp",
            "url": "http://localhost:8000",
            //"url": "https://sdc-dev.astron.nl", // uncomment if you don't want to run the adex-backend locally
        }
        : {
            "name": "sdc.astron.nl",
            "type": "primary_dp",
            "url": "https://sdc.astron.nl",
        };

const default_alta_backend =
    process.env.NODE_ENV === "development"
        ? {
            "name": "localhost:8000",
            "url": "http://localhost:8000",
       }
        : {
            "name": "sdc-dev.astron.nl",
            "url": "https://sdc-dev.astron.nl",
        };

export const initialState = {
        config_name       : "adex-labs",
        status_adex       : "unfetched",
        number_of_dataproducts : 0,
        adex_backend : default_adex_backend,
        adex_ra: "10",
        adex_dec: "40",
        adex_fov: "70",
        level : "2",
        dataproduct_subtype: "continuumMF",
        status_primary : "unfetched",
        status_activity : "unfetched",
        data_limit: 10000,
        cone_search : true,
        labels_enabled      : false,
        label_field         : 'name',
        // collection          : {name: ""}

        // ALTA
        status_alta       : "unfetched",
        alta_backend      : default_alta_backend,
        alta_count        : 0,

        // SPACE WEATHER (Pithia)
        status_sw       : "unfetched",
        sw_count        : 0,
        sw_date         : new Date(),
        sw_starttime    : "00:00:00",
        sw_endtime      : "23:59:00",
}

export const reducer = (state, action) => {
    if (action.type !== ALADIN_SET_MOUSE) {
        console.log('action: ' + action.type)
    }

    switch (action.type) {

        case SET_CONFIG_NAME:
            return {
                ...state,
                config_name: action.config_name
            };

        case SET_BACKEND_CONFIG:
            return {
                ...state,
                backend_config: action.backend_config
            };

        case RELOAD_CONFIG:
            return {
                ...state,
                reload_config: action.reload_config
            };

        case SET_STATUS_ADEX:
            return {
                ...state,
                status_adex: action.status_adex
            };

        case SET_FETCHED_ADEX:
            return {
                ...state,
                fetched_adex: action.fetched_adex
            };

        case RELOAD_ADEX:
            return {
                ...state,
                reload_adex: action.reload_adex
            };


        case SET_ADEX_BACKEND:
            return {
                ...state,
                adex_backend: action.adex_backend
            };

        case SET_NUMBER_OF_DATAPRODUCTS:
            return {
                ...state,
                number_of_dataproducts: action.number_of_dataproducts
            }

        case ADEX_RA:

            return {
                ...state,
                adex_ra: action.adex_ra
            };

        case ADEX_DEC:
            return {
                ...state,
                adex_dec: action.adex_dec
            };

        case ADEX_FOV:
            return {
                ...state,
                adex_fov: action.adex_fov
            };

        case SET_LEVEL:
            return {
                ...state,
                level: action.level
            };

        case SET_COLLECTION:
            return {
                ...state,
                collection: action.collection
            };

        case SET_DATAPRODUCT_TYPE:
            return {
                ...state,
                dataproduct_type: action.dataproduct_type
            };

        case SET_DATAPRODUCT_SUBTYPE:
            return {
                ...state,
                dataproduct_subtype: action.dataproduct_subtype
            };

        case ALADIN_RELOAD:
            //console.log('action: ALADIN_RELOAD')
            return {
                ...state,
                aladin_reload: action.aladin_reload
            };

        case ALADIN_SET_MOUSE:

            return {
                ...state,
                aladin_mouse: action.aladin_mouse
            };

        case SET_SELECTED_SURVEY:
            return {
                ...state,
                selected_survey: action.selected_survey
            };

        case SET_SELECTED_OBJECT:
            console.error(action.selected_object)
            return {
                ...state,
                selected_object: action.selected_object,
                //selected_activity_id: action.selected_object.activity_id,
            };

        case SET_SELECTED_PRIMARY:
            return {
                ...state,
                selected_primary: action.selected_primary,
            };

        case SET_STATUS_PRIMARY:
            return {
                ...state,
                status_primary: action.status_primary,
            };

        case SET_PRIMARY_DP_URL:
            return {
                ...state,
                primary_dp_url: action.primary_dp_url,
            };

        case SET_SELECTED_ACTIVITY:
            return {
                ...state,
                selected_activity: action.selected_activity,
            };

        case SET_STATUS_ACTIVITY:
            return {
                ...state,
                status_activity: action.status_activity,
            };

        case SET_ACTIVITY_URL:
            return {
                ...state,
                activity_url: action.activity_url,
            };

        case SET_VIEWED_OBJECT:
            return {
                ...state,
                viewed_object: action.viewed_object,
            };

        case SET_DATA_LIMIT:

            return {

                ...state,
                data_limit: action.data_limit
            };

        case SET_ANCILLARY_DATA:
            return {
                ...state,
                ancillary_data: action.ancillary_data
            };

        case SET_SIBLING_DATA:
            return {
                ...state,
                sibling_data: action.sibling_data
            };

        case CONE_SEARCH:
            return {
                ...state,
                cone_search: action.cone_search
            };

        case SHOW_LABELS:
            return {
                ...state,
                show_labels: action.show_labels
            };

        case SET_LABEL_FIELD:
            return {
                ...state,
                label_field: action.label_field
            };

        case SET_SIMBAD_ENABLED:
            return {
                ...state,
                simbad_enabled: action.simbad_enabled
            };

        case SET_ALTA_BACKEND:
            return {
                ...state,
                alta_backend: action.alta_backend
            };

        case SET_STATUS_ALTA:
            return {
                ...state,
                status_alta: action.status_alta
            };

        case SET_ALTA_DATA:
            return {
                ...state,
                alta_data: action.alta_data
            };

        case SET_ALTA_COUNT:
            return {
                ...state,
                alta_count: action.alta_count
            }

        case RELOAD_ALTA:
            return {
                ...state,
                reload_alta: action.reload_alta
            };

        case SET_STATUS_SW:
            return {
                ...state,
                status_sw: action.status_sw
            };

        case SET_SW_DATA:
            return {
                ...state,
                sw_data: action.sw_data
            };

        case SET_SW_DATE:
            return {
                ...state,
                sw_date: action.sw_date
            };

        case SET_SW_STARTTIME:
            return {
                ...state,
                sw_starttime: action.sw_starttime
            };

        case SET_SW_ENDTIME:
            return {
                ...state,
                sw_endtime: action.sw_endtime
            };

        case SET_SW_COUNT:
            return {
                ...state,
                sw_count: action.sw_count
            }

        case RELOAD_SW:
            return {
                ...state,
                reload_sw: action.reload_sw
            };
            
        default:
            return state;
    }
};