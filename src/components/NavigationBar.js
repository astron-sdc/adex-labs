import React from 'react';

import { Navbar, Nav } from 'react-bootstrap';
import '../App.css';
import logo from '../logo.ico';
import { NavLink, Link } from "react-router-dom"

import { useGlobalReducer } from '../contexts/GlobalContext';

export default function NavigationBar() {
    const [ my_state , my_dispatch] = useGlobalReducer()

    let render_skyview
    try {
        if (my_state.status_adex !== 'unfetched') {
            render_skyview = <Nav.Link as={NavLink} to="/skyview">SkyView</Nav.Link>
        } else {
            render_skyview = <Nav.Link as={NavLink} disabled={true} to="/skyview">SkyView</Nav.Link>
        }
    } catch (e) {
    }

    let render_configuration
    try {
        if (my_state.backend_config) {
            render_configuration = <Nav.Link as={NavLink} to="/configuration">Configuration</Nav.Link>
        } else {
            render_configuration = <Nav.Link as={NavLink} disabled={true} to="/configuration">Configuration</Nav.Link>
        }
    } catch (e) {
    }

    let render_benchmarking
    try {
        if (my_state.backend_config) {
            render_benchmarking = <Nav.Link as={NavLink} to="/benchmarking">(experiments)</Nav.Link>
        } else {
            render_benchmarking = <Nav.Link as={NavLink} disabled={true} to="/benchmarking">(experiments)</Nav.Link>
        }
    } catch (e) {
    }

    let render_dataproduct
    try {
        if (my_state.selected_object) {
            render_dataproduct =
                <Nav.Link as={NavLink} to={"/dataproduct/" + my_state.selected_object.id}>Dataproduct</Nav.Link>
        }
    } catch (e) {
    }

    let render_process
    try {
        if (my_state.selected_activity) {
            render_process =
                <Nav.Link as={NavLink} to={"/process/" + my_state.selected_object.activity_id}>Process</Nav.Link>
        }
    } catch (e) {
    }

    return (
        <Navbar bg="dark" variant="dark">

            <img alt='' src={logo} width="30" height="30" className="d-inline-block align-top"/>

            <Nav className="mr-auto">
                <Nav.Link as={NavLink} to="/">ADEX-labs</Nav.Link>
                {render_skyview}
                {render_process}
                {render_dataproduct}
                {render_configuration}
                {/*                  {render_benchmarking}
                <Nav.Link as={NavLink} to="/alta">ALTA</Nav.Link>*/}
{/*                <Nav.Link as={NavLink} to="/spaceweather">Spaceweather</Nav.Link>*/}
            </Nav>

        </Navbar>

    );
}