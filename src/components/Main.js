
import '../App.css';

import {
    BrowserRouter as Router,
    Routes,
    Route, useParams,
} from "react-router-dom";

import NavigationBar from './NavigationBar';
import WelcomePage from '../pages/welcome/WelcomePage';
import BenchmarkingPage from '../pages/benchmarking/BenchmarkingPage';
import SkyViewPage from '../pages/skyview/SkyViewPage';
import ConfigurationPage from "../pages/configuration/ConfigurationPage";
import FetchADEXData from '../data/FetchADEXData';
import AncillaryPage from "../pages/ancillary/AncillaryPage";
import DataProductPage from "../pages/dataproduct_details/DataProductPage";
import FetchConfig from "../data/FetchConfig";
import ProcessPage from "../pages/process_details/ProcessPage";
import ALTAPage from "../pages/alta/ALTAPage";
import SpaceWeatherPage from "../pages/spaceweather/SpaceWeatherPage";

export default function Main() {

    FetchConfig()
    FetchADEXData(false)

    return (
        <Router basename="adex-labs">
            <div>
                <NavigationBar/>

                <Routes>
                    <Route path="/" element={<WelcomePage />} />

                    <Route path="skyview/" element={<SkyViewPage />} />
                    <Route path="configuration/" element={<ConfigurationPage />} />
                    <Route path="benchmarking/" element={<BenchmarkingPage />} />
                    <Route path="alta/" element={<ALTAPage />} />
                    <Route path="spaceweather/" element={<SpaceWeatherPage />} />
                    <Route path="ancillary/" element={<AncillaryPage />} />

                    <Route path="dataproduct/:id" element={<DataproductDetails />} />
                    <Route path="process/:id" element={<ProcessDetails />} />

                </Routes>
            </div>
            <footer>
                <small>version 3 Jan 2025</small>
            </footer>
        </Router>

    );
}

function DataproductDetails() {

    let { page, id } = useParams();
    return <DataProductPage object_id={id} />
}

function ProcessDetails() {
    let { page, id } = useParams();
    return <ProcessPage activity_id={id} />
}