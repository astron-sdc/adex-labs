import React, {useEffect} from "react";
import { Container, Row, Col, Card, Table } from 'react-bootstrap';

import LoadingSpinner from '../../components/LoadingSpinner';
import { useGlobalReducer } from '../../contexts/GlobalContext';


export default function AncillaryPage(props) {

    const [ my_state, my_dispatch] = useGlobalReducer()

    return (
        <div>
            <Container fluid>
                <Row>
                    <Col sm={12} md={12} lg={12}>
                        <Card>
                            Stuff
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}