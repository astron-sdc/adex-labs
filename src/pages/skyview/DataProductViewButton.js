import React from 'react';
import { Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {
    SET_PRIMARY_DP_URL,
} from '../../contexts/GlobalStateReducer'

import { useNavigate  } from 'react-router-dom';

export default function DataProductViewButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()
    let navigate = useNavigate();

    if (!my_state.selected_object) {
        return null
    }

    const handleClick = (selected_object) => {
        // load primary dataproduct_details for selected dataset_id
        let dataproduct_id = selected_object.id
        if (props.id) {
            dataproduct_id = props.id
        }

        let url = my_state.adex_backend.url + '/adex_backend/api/v1/primary_dp/?id='+dataproduct_id

        // this triggers a fetch
        my_dispatch({type: SET_PRIMARY_DP_URL, primary_dp_url: url})

        let new_route =  '/dataproduct/' + dataproduct_id
        navigate(new_route)
    }

    return <Button className="select-btn" variant="outline-primary" onClick={() => handleClick(my_state.selected_object)}>DataProduct</Button>
}