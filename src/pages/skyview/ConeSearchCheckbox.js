import React from 'react';
import { Form, FormControl, Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';

import {CONE_SEARCH, RELOAD_ADEX} from '../../contexts/GlobalStateReducer'

export default function ConeSearchCheckbox(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (event) => {
        let checked = event.target.checked
        my_dispatch({type: CONE_SEARCH, cone_search: checked})
        my_dispatch({type: RELOAD_ADEX, reload_adex: !my_state.reload_adex})
    }

    return (

            <Form.Check
                inline
                id="labels"
                label="Cone Search"
                checked = {my_state.cone_search}
                onClick={handleClick}
            />

    );


}
