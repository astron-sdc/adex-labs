import React from 'react';
import {Card, Col, Row } from 'react-bootstrap'

import ProcessViewButton from "./ProcessViewButton";
import DataProductViewButton from "./DataProductViewButton";
import {useGlobalReducer} from "../../contexts/GlobalContext";

export default function DetailsButtonsPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    return (
        <div className="App">
            <Card>
                <Card.Body align={"left"}>
                    <Card.Subtitle className="mb-2 text-muted">Details</Card.Subtitle>

                    <Row>
                        <Col sm={5} md={5} lg={5}> <ProcessViewButton /></Col>
                        <Col sm={5} md={5} lg={5}> <DataProductViewButton /></Col>
                    </Row>
                </Card.Body>
            </Card>
        </div>
    );

}

