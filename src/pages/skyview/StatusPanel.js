import React from 'react';
import {Container, Card, Col, Row, Table} from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';
import { toHMSLabel, toFOVLabel } from '../../utils/coordinates'


export default function StatusPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const renderRADec = (ra,dec) => {
        try {
            let ra_label = Number(ra).toFixed(2)
            let dec_label = Number(dec).toFixed(2)
            let radec_label = ra_label + ', ' + dec_label
            return <div>{toHMSLabel(radec_label)} </div>
        } catch (e) {
            alert(e)
            return <div>n/a</div>
        }
    }

    return (
        <div className="App">
            <Card>
                <Card.Body align={"left"}>
                    <Card.Subtitle className="mb-2 text-muted">Status</Card.Subtitle>
                    <Row><Col sm={4} md={4} lg={4}>Backend:</Col><Col sm={8} md={8} lg={8}> {my_state.adex_backend.name}</Col></Row>
                    <Row><Col sm={4} md={4} lg={4}>Status :</Col><Col sm={8} md={8} lg={8}>  {my_state.status_adex}, {my_state.number_of_dataproducts} dps</Col></Row>
                    <Row><Col sm={4} md={4} lg={4}>Position:</Col><Col sm={8} md={8} lg={8}>  {renderRADec(my_state.adex_ra, my_state.adex_dec)} </Col></Row>
                </Card.Body>
            </Card>
        </div>
    );

}

