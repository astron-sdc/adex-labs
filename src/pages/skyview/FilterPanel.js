import React from 'react';
import {Card, Col, Row } from 'react-bootstrap'

import SelectCollectionButton from "./SelectCollectionButton";
import SelectDataProductTypeButton from "./SelectDataProductTypeButton";

export default function FilterPanel(props) {

    return (
        <div className="App">
            <Card>
                <Card.Body align={"left"}>
                    <Card.Subtitle className="mb-2 text-muted">Filters</Card.Subtitle>
                    <Row><Col>Collection:</Col></Row>
                    <Row><Col><SelectCollectionButton /></Col></Row>

                    <Row><Col>DataProduct Type:</Col></Row>
                    <Row><Col><SelectDataProductTypeButton /></Col></Row>
                </Card.Body>
            </Card>
        </div>
    );

}

