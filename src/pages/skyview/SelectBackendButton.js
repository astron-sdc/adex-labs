import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {RELOAD_ADEX, SET_ADEX_BACKEND} from '../../contexts/GlobalStateReducer'

export default function SelectBackendButton() {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (backend) => {
        my_dispatch({type: SET_ADEX_BACKEND, adex_backend: backend})
        my_dispatch({type: RELOAD_ADEX, reload_adex: !my_state.reload_adex})
    }

    let renderDropdownItems
    /*
    if (static_config.focus.benchmarking) {
        renderDropdownItems = static_config.focus.benchmarking.map(backend => {
            return <Dropdown.Item onClick={() => handleClick(backend)}>{backend.name}</Dropdown.Item>
        })
    }
*/
    if (my_state.backend_config) {
        renderDropdownItems = my_state.backend_config.config.focus.backends.map(backend => {
            return <Dropdown.Item onClick={() => handleClick(backend)}>{backend.name}</Dropdown.Item>
        })
    }

    return <Dropdown>
        <Dropdown.Toggle variant="outline-primary" id="dropdown-basic">
            {my_state.adex_backend.name}
        </Dropdown.Toggle>
        <Dropdown.Menu>
            {renderDropdownItems}
         </Dropdown.Menu>
    </Dropdown>


}