import React from 'react';
import { Form, FormControl, Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';

import {SHOW_LABELS, ALADIN_RELOAD} from '../../contexts/GlobalStateReducer'

export default function ShowLabelsCheckbox(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (event) => {
        let checked = event.target.checked
        my_dispatch({type: SHOW_LABELS, show_labels: checked})
        my_dispatch({type: ALADIN_RELOAD, aladin_reload: !my_state.aladin_reload})
    }

    return (

            <Form.Check
                inline
                id="labels"
                label="Show Labels"
                checked = {my_state.show_labels}
                onClick={handleClick}
            />

    );


}
