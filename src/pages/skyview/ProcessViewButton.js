import React from 'react';
import { Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {
    SET_ACTIVITY_URL,
} from '../../contexts/GlobalStateReducer'

import { useNavigate   } from 'react-router-dom';

export default function ProcessViewButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()
    let navigate = useNavigate();

    if (!my_state.selected_object) {
        return null
    }

    const handleClick = (selected_activity_id) => {

        // load primary dataproduct_details for selected dataset_id
        let url = my_state.adex_backend.url + '/adex_backend/api/v1/activity/'+selected_activity_id

        // this triggers a fetch
        my_dispatch({type: SET_ACTIVITY_URL, activity_url: url})

        let new_route =  '/process/' + selected_activity_id
        navigate(new_route)
    }

    return <Button className="select-btn" variant="outline-primary" onClick={() => handleClick(my_state.selected_object.activity_id)}>Process</Button>
}