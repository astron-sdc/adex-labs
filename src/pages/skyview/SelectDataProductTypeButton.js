import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {RELOAD_ADEX, SET_DATAPRODUCT_TYPE} from '../../contexts/GlobalStateReducer'

export default function SelectDataProductTypeButton() {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (dataproduct_type) => {
        my_dispatch({type: SET_DATAPRODUCT_TYPE, dataproduct_type: dataproduct_type})
        my_dispatch({type: RELOAD_ADEX, reload_adex: !my_state.reload_adex})
    }

    let renderDropdownItems

    try {
        renderDropdownItems = my_state.collection.dp_types.map(dp_type => {
            return <Dropdown.Item onClick={() => handleClick(dp_type)}>{dp_type}</Dropdown.Item>
        })
    } catch (e) {
        return null
    }

    return <Dropdown>
        <Dropdown.Toggle variant="outline-primary" id="dropdown-basic">
            {my_state.dataproduct_type}
        </Dropdown.Toggle>

        <Dropdown.Menu>
            {renderDropdownItems}
        </Dropdown.Menu>
    </Dropdown>


}