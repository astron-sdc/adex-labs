import React from 'react';
import {Card, Col, Row } from 'react-bootstrap'

import SelectCollectionButton from "./SelectCollectionButton";
import SelectDataProductTypeButton from "./SelectDataProductTypeButton";
import SurveyFilterButton from "./SurveyFilterButton";
import ShowLabelsCheckbox from "./ShowLabelsCheckbox";
import SelectLabelButton from "./SelectLabelButton";
import SimbadCheckbox from "./SimbadCheckbox";

export default function AladinBackgroundPanel(props) {

    return (
        <div className="App">
            <Card>
                <Card.Body align={"left"}>
                    <Card.Subtitle className="mb-2 text-muted">Aladin Background</Card.Subtitle>
                    <Row><Col>Background Survey (HiPS) :</Col></Row>
                    <Row><Col><SurveyFilterButton /></Col></Row>

                    <Row><Col><ShowLabelsCheckbox /><SimbadCheckbox /></Col></Row>
                    <Row><Col><SelectLabelButton /></Col></Row>
                </Card.Body>
            </Card>
        </div>
    );

}

