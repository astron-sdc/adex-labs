import React from 'react';
import {Container, Card, Col, Row, Table} from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';

import StatusPanel from "./StatusPanel";
import FilterPanel from "./FilterPanel";
import AladinBackgroundPanel from "./AladinBackgroundPanel";
import CommandPanel from "./CommandPanel";
import DetailsButtonsPanel from "./DetailsButtonsPanel";

export default function LeftPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()


    return (
        <div className="App">
            <StatusPanel />
            <DetailsButtonsPanel />
            <FilterPanel />
            <AladinBackgroundPanel />
            <CommandPanel />
        </div>
    );

}

