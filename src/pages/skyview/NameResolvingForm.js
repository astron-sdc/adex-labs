import React, { useContext, useState, useEffect } from "react";
import { Button, Modal, Form, Table, Card } from "react-bootstrap";

import { useGlobalReducer } from '../../contexts/GlobalContext';

import {
    ADEX_RA,
    ADEX_DEC,
    RELOAD_ADEX
} from '../../contexts/GlobalStateReducer'

import { getCrosshairsIcon, getMapIcon } from '../../utils/styling'

export default function NameResolvingForm(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const [targetName, setTargetName] = useState();
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function executeNameResolving(e) {

        let url = my_state.adex_backend.url + "/adex_backend/get-sky-coordinates/?target_name=" + targetName
        //alert(url)
        fetch(url)
            .then((result) => {
                return result.json().then((json) => {
                    my_dispatch({type: ADEX_RA, adex_ra: json.ra})
                    my_dispatch({type: ADEX_DEC, adex_dec: json.dec})
                    my_dispatch({type: RELOAD_ADEX, reload_adex: !my_state.reload_adex})
                });
            })
            .catch(function () {
                alert("fetch to " + url + " failed.");
            })
        setShow(false)

    };

    function close(){
        setShow(false)
    }

    if (my_state.adex_backend.type !== 'fastapi') {
        let NameResolvingForm = <div>
            <Modal.Header>
                <Modal.Title>{getCrosshairsIcon()}{' '}Name Resolving</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Label>Name</Form.Label>
                <Form.Control as="textarea" rows={1} value={targetName}
                              onChange={e => setTargetName(e.target.value)}>
                </Form.Control>

                <Form.Text className="text-muted">
                    The name of an astronomical target
                </Form.Text>
            </Modal.Body>
            <Modal.Footer>
                <Button className="custom-btn" variant="outline-primary" onClick={executeNameResolving}>
                    OK
                </Button>
                <Button className="custom-btn" variant="outline-danger" onClick={close}>Cancel
                </Button>
            </Modal.Footer>
        </div>

        return (
            <div>
                <Button
                    type="button" className="custom-btn"
                    variant="outline-primary"
                    onClick={handleShow}
                    {...props}>
                    {getCrosshairsIcon('black')}&nbsp;Resolve Target
                </Button>

                <Modal size="lg" show={show} onHide={handleClose}>
                    {NameResolvingForm}
                </Modal>
            </div>
        )
    } else {
        return null
    }
}
