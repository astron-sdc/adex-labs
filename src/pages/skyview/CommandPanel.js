import React from 'react';
import {Card, Col, Row } from 'react-bootstrap'

import NameResolvingForm from "./NameResolvingForm";
import ResetButton from "./ResetButton";
import RefreshButton from "./RefreshButton";
import {useGlobalReducer} from "../../contexts/GlobalContext";
import ConeSearchCheckbox from "./ConeSearchCheckbox";

export default function CommandPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    return (
        <div className="App">
            <Card>
                <Card.Body align={"left"}>
                    <Card.Subtitle className="mb-2 text-muted">Functions</Card.Subtitle>
                    <Row>
                        <Col sm={5} md={5} lg={5}><NameResolvingForm /></Col>
                        <Col sm={5} md={5} lg={5}><ConeSearchCheckbox /></Col>
                    </Row>

                    <Row>
                        <Col sm={5} md={5} lg={5}><ResetButton /></Col>
                        <Col sm={5} md={5} lg={5}><RefreshButton /></Col>
                    </Row>

                </Card.Body>
            </Card>
        </div>
    );

}

