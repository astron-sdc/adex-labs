import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {SET_ADEX_BACKEND} from '../../contexts/GlobalStateReducer'

export default function SelectBackendButton() {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (backend) => {
        my_dispatch({type: SET_ADEX_BACKEND, adex_backend: backend})
    }

    let renderDropdownItems
    if (my_state.backend_config) {
        if (my_state.backend_config.config) {
            renderDropdownItems = my_state.backend_config.config.focus.backends.map(backend => {
                if (backend.type !== 'fastapi') {
                    return <Dropdown.Item onClick={() => handleClick(backend)}>{backend.name}</Dropdown.Item>
                }

            })
        }
    }


    return <Dropdown>
        <Dropdown.Toggle variant="outline-primary" id="dropdown-basic">
            {my_state.adex_backend.name}
        </Dropdown.Toggle>
        <Dropdown.Menu>
            {renderDropdownItems}
         </Dropdown.Menu>
    </Dropdown>


}