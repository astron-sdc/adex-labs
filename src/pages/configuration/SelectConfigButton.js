import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {RELOAD_CONFIG, SET_CONFIG_NAME} from '../../contexts/GlobalStateReducer'

export default function SelectConfigButton() {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (config_name) => {
        my_dispatch({type: SET_CONFIG_NAME, config_name: config_name})
        my_dispatch({type: RELOAD_CONFIG, reload_config: !my_state.reload_config})
    }


    return <Dropdown>
        <Dropdown.Toggle variant="outline-primary" id="dropdown-basic">
            {my_state.config_name}
        </Dropdown.Toggle>

        <Dropdown.Menu>
            <Dropdown.Item onClick={() => handleClick('adex-gui')}>adex-gui</Dropdown.Item>
            <Dropdown.Item onClick={() => handleClick('adex-labs')}>adex-labs</Dropdown.Item>
        </Dropdown.Menu>
    </Dropdown>


}