import React, {useEffect} from "react";
import { Row, Col, Card, Table } from 'react-bootstrap';

import { useGlobalReducer } from '../../contexts/GlobalContext';
import LoadingSpinner from '../../components/LoadingSpinner';
import SelectBackendButton from "./SelectBackendButton";
import SelectConfigButton from "./SelectConfigButton";
import DataLimitBox from "./DataLimitBox";


export default function ConfigurationPage(props) {

    const [ my_state, my_dispatch] = useGlobalReducer()


    let renderFields

    try {
        if (my_state.backend_config) {
            let config_fields = my_state.backend_config.fields.fields
            let fields = config_fields.map(field => {
                let j = JSON.stringify(field)
                return <Row><Col>{j}</Col></Row>
            })
            renderFields = <div>
                <Card>
                    <Table>
                        <h5>Fields</h5>{fields}
                    </Table>
                </Card>
            </div>
        }
    } catch (e) {
    }

    let renderGroups
    try {
        if (my_state.backend_config) {
            let groups = JSON.stringify(my_state.backend_config.groups)
            renderGroups = <div>
                <Card>
                    <Table>
                        <h5>Groups</h5>{groups}
                    </Table>
                </Card>
            </div>
        }
    } catch (e) {
    }

    let renderSettings
    try {
        if (my_state.backend_config) {
            let settings = JSON.stringify(my_state.backend_config.settings)
            renderSettings = <div>
                <Card>
                    <Table>
                        <h5>Settings (ESAP style)</h5>{settings}
                    </Table>
                </Card>
            </div>
        }
    } catch (e) {
    }

    let renderConfig
    try {
        if (my_state.backend_config) {
            let settings = JSON.stringify(my_state.backend_config.config)
            renderConfig = <div>
                <Card>
                    <Table>
                        <h5>Config (defaults)</h5>{settings}
                    </Table>
                </Card>
            </div>
        }
    } catch (e) {
    }

    return (
        <div className="App">
            <Card className='text-left'>
                <Card.Body>
                    <h5>ADEX Configuration</h5>

                    <Table>
                        <Row>
                            <Col sm={2} md={2} lg={2}>Choose ADEX Backend    :</Col>
                            <Col sm={2} md={2} lg={2}>Choose Configuration   :</Col>
                        </Row>
                        <Row>
                            <Col sm={2} md={2} lg={2}><SelectBackendButton /></Col>
                            <Col sm={2} md={2} lg={2}><SelectConfigButton /></Col>
                        </Row>
                        <Row><Col>Max Records       : <DataLimitBox/></Col></Row>
                    </Table>
                    <ul>
                        <li><a href={"https://support.astron.nl/confluence/display/SDCP/ADEX+Configuration"} target={"_blank"}>documentation</a></li>
                    </ul>

                    {renderFields}
                    {renderGroups}
                    {renderConfig}
                </Card.Body>
            </Card>
        </div>
    );
}