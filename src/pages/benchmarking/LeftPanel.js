import React from 'react';
import {Container, Card, Col, Row, Table} from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';
import { toHMSLabel, toFOVLabel } from '../../utils/coordinates'
import DataLimitBox from './DataLimitBox'
import RefreshButton from "./RefreshButton";
import ResetButton from "./ResetButton";

import SelectBackendButton from "./SelectBackendButton"

import SelectCollectionButton from "./SelectCollectionButton";
import SelectDataProductTypeButton from "./SelectDataProductTypeButton";
import SelectDataProductSubTypeButton from "./SelectDataProductSubTypeButton";
import SurveyFilterButton from "./SurveyFilterButton";
import NameResolvingForm from "./NameResolvingForm";

export default function LeftPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const renderRADec = (ra,dec) => {
        try {
            let ra_label = Number(ra).toFixed(2)
            let dec_label = Number(dec).toFixed(2)
            let radec_label = ra_label + ', ' + dec_label
            return <div>{toHMSLabel(radec_label)} </div>
        } catch (e) {
            alert(e)
            return <div>n/a</div>
        }
    }

    return (
        <div className="App">
            <Card>

                <Card.Body align={"left"}>

                    <Table>
                        <Row><Col sm={4} md={4} lg={4}>Backend:</Col><Col sm={8} md={8} lg={8}> {my_state.adex_backend.name}</Col></Row>
                        <Row><Col sm={4} md={4} lg={4}>Status :</Col><Col sm={8} md={8} lg={8}>  {my_state.status_adex}</Col></Row>
                        <Row><Col sm={4} md={4} lg={4}>DPS:</Col><Col sm={8} md={8} lg={8}>  {my_state.number_of_dataproducts}</Col></Row>
                        <Row><Col sm={4} md={4} lg={4}>Position:</Col><Col sm={8} md={8} lg={8}>  {renderRADec(my_state.adex_ra, my_state.adex_dec)}</Col></Row>
                        <Row><Col sm={4} md={4} lg={4}>FoV:</Col><Col sm={8} md={8} lg={8}>  {Math.round(my_state.adex_fov*100)/100} deg</Col></Row>
                        <Row><Col sm={4} md={4} lg={4}>Object:</Col><Col sm={8} md={8} lg={8}> {my_state.selected_object}</Col></Row>
                        <Row></Row>

                        <hr></hr>

                        <Row><Col>Choose ADEX Backend    :</Col></Row>
                        <Row><Col><SelectBackendButton /></Col></Row>
                        <Row><Col>Background Survey (HiPS) :</Col></Row>
                        <Row><Col><SurveyFilterButton /></Col></Row>

                        <Row><Col>Collection:</Col></Row>
                        <Row><Col><SelectCollectionButton /></Col></Row>

{/*                        <Row><Col>DataProduct Type:</Col></Row>
                        <Row><Col><SelectDataProductTypeButton /></Col></Row>*/}

                        <Row><Col>DataProduct SubType:</Col></Row>
                        <Row><Col><SelectDataProductSubTypeButton /></Col></Row>
                        <Row><Col>Max Records       : <DataLimitBox/></Col></Row>

                        <Row><Col><NameResolvingForm /></Col></Row>
                        <Row><Col><ResetButton /></Col></Row>
                        <Row><Col><RefreshButton /></Col></Row>
                    </Table>
                </Card.Body>
            </Card>
        </div>
    );

}

