import React from 'react';
import { Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';

import {
    ADEX_RA,
    ADEX_DEC,
    ADEX_FOV,
    SET_COLLECTION,
    SET_DATAPRODUCT_TYPE,
    SET_DATAPRODUCT_SUBTYPE,
    SET_SELECTED_SURVEY,
    RELOAD_ADEX, SET_STATUS_ADEX} from '../../contexts/GlobalStateReducer'

import { getReloadIcon } from '../../utils/styling'

export default function ResetButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = () => {
        my_dispatch({type: ADEX_RA, adex_ra: my_state.backend_config.config.skyviews.defaults.ra})
        my_dispatch({type: ADEX_DEC, adex_dec: my_state.backend_config.config.skyviews.defaults.dec})
        my_dispatch({type: ADEX_FOV, adex_fov: my_state.backend_config.config.skyviews.defaults.fov})

        my_dispatch({type: SET_COLLECTION, collection: my_state.backend_config.config.skyviews.defaults.collection})
        my_dispatch({type: SET_DATAPRODUCT_TYPE, dataproduct_type: my_state.backend_config.config.skyviews.defaults.dataproduct_type})
        my_dispatch({type: SET_DATAPRODUCT_SUBTYPE, dataproduct_subtype: my_state.backend_config.config.skyviews.defaults.dataproduct_subtype})
        my_dispatch({type: SET_SELECTED_SURVEY, selected_survey: my_state.backend_config.config.skyviews.defaults.selected_survey})

        my_dispatch({type: SET_STATUS_ADEX, status_adex: 'unfetched'})
        my_dispatch({type: RELOAD_ADEX, reload_adex: !my_state.reload_adex})
    }

    return <Button variant="outline-primary" onClick={() => handleClick()}>{getReloadIcon()}&nbsp;Reset View</Button>
}