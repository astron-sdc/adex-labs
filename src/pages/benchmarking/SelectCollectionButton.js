import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {RELOAD_ADEX, SET_COLLECTION, SET_DATAPRODUCT_TYPE, SET_SELECTED_OBJECT, SET_SIBLING_DATA, SET_ANCILLARY_DATA} from '../../contexts/GlobalStateReducer'

export default function SelectCollectionButton() {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (collection) => {
        // reset everything when a new collection is selected
        my_dispatch({type: SET_COLLECTION, collection: collection})
        my_dispatch({type: SET_DATAPRODUCT_TYPE, dataproduct_type: collection.dp_types[0]})
        my_dispatch({type: SET_SELECTED_OBJECT, selected_object: undefined})
        my_dispatch({type: SET_SIBLING_DATA, sibling_data: undefined})
        my_dispatch({type: SET_ANCILLARY_DATA, ancillary_data: undefined})
        my_dispatch({type: RELOAD_ADEX, reload_adex: !my_state.reload_adex})
    }

    let renderDropdownItems
    if (my_state.backend_config) {
        if (my_state.backend_config.config) {
            renderDropdownItems = my_state.backend_config.config.skyviews.collections.map(collection => {
                return <Dropdown.Item onClick={() => handleClick(collection)}>{collection.name}</Dropdown.Item>
            })
        }
    }

    return <Dropdown>
        <Dropdown.Toggle variant="outline-primary" id="dropdown-basic">
            {my_state.collection.name}
        </Dropdown.Toggle>

        <Dropdown.Menu>
            {renderDropdownItems}
        </Dropdown.Menu>
    </Dropdown>


}