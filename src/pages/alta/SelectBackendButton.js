import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import { SET_ALTA_BACKEND, SET_STATUS_ALTA, SET_ALTA_COUNT} from '../../contexts/GlobalStateReducer'

export default function SelectBackendButton() {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (backend) => {
        my_dispatch({type: SET_ALTA_BACKEND, alta_backend: backend})
        my_dispatch({type: SET_STATUS_ALTA, status_alta: "unfetched"})
        my_dispatch({type: SET_ALTA_COUNT, alta_count: 0})
    }

    let renderDropdownItems
    let alta_backends = [
        {
            "name": "localhost:8000 (dev)",
            "url": "http://localhost:8000",
        },
        {
            "name": "SURF Research Cloud (dev/test)",
            "url": "http://145.38.187.31",
        },
        {
            "name": "sdc-dev.astron.nl (test/acceptance)",
            "url": "https://sdc-dev.astron.nl",
        },
        {
            "name": "alta-acc.astron.nl (unreachable, cors)",
            "url": "https://alta-acc.astron.nl",
        },
        {
            "name": "alta.astron.nl (unreachable, cors)",
            "url": "https://alta.astron.nl",
        },
    ]


    renderDropdownItems = alta_backends.map(backend => {
        return <Dropdown.Item onClick={() => handleClick(backend)}>{backend.name}</Dropdown.Item>
    })


    return <Dropdown>
        <Dropdown.Toggle variant="outline-primary" id="dropdown-basic">
            {my_state.alta_backend.name}
        </Dropdown.Toggle>
        <Dropdown.Menu>
            {renderDropdownItems}
         </Dropdown.Menu>
    </Dropdown>


}