import React from 'react';
import {Container, Card, Col, Row, Table} from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';

import SelectBackendButton from "./SelectBackendButton"
import ReleasesButton from "./ReleasesButton";
import ProjectsButton from "./ProjectsButton";
import ObservationsButton from "./ObservationsButton";
import ActivitiesButton from "./ActivitiesButton";
import IngestsButton from "./IngestsButton";
import PipelinesButton from "./PipelinesButton";
import DataproductsButton from "./DataproductsButton";
import ProcessesButton from "./ProcessesButton";
import GetStorageButton from "./GetStorageButton";
import GetFreezeButton from "./GetFreezeButton";
import GetScrubButton from "./GetScrubButton";
import GetPruneButton from "./GetPruneButton";
import GetPreparedColdStorageButton from "./GetPreparedColdStorageButton";
import GetMarkedForDeletionButton from "./GetMarkedForDeletionButton";
import ColdStorageButton from "./ColdStorageButton";

export default function LeftPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    return (
        <div className="App">
            <Card>

                <Card.Body align={"left"}>

                    <Table>
                        <Row><Col sm={4} md={4} lg={4}>Backend:</Col><Col sm={8} md={8} lg={8}> {my_state.alta_backend.name}</Col></Row>
                        <Row><Col sm={4} md={4} lg={4}>Status :</Col><Col sm={8} md={8} lg={8}>  {my_state.status_alta}</Col></Row>
                        <Row><Col sm={4} md={4} lg={4}>Count:</Col><Col sm={8} md={8} lg={8}>  {my_state.alta_count}</Col></Row>
                      <Row></Row>

                        <hr></hr>

                        <Row><Col>Choose ALTA Backend    :</Col></Row>
                        <Row><Col><SelectBackendButton /></Col></Row>
                        <hr></hr>
                        <Row><Col>Select Data:</Col></Row>
                        <Row>
                            <Col>
                                <ReleasesButton />
                                <ProjectsButton />
                                <ProcessesButton />
                                <ActivitiesButton />
                                <ObservationsButton />
                                <PipelinesButton />
                                <DataproductsButton />
                                <IngestsButton />
                            </Col>
                        </Row>
                        <hr></hr>
                        <Row><Col>Select Function:</Col></Row>
                        <Row>
                            <Col>
                                <ColdStorageButton />
                                <GetStorageButton />
                                <GetFreezeButton />
                                <GetScrubButton />
                                <GetPruneButton />
                                <GetPreparedColdStorageButton />
                                <GetMarkedForDeletionButton />
                            </Col>
                        </Row>
                    </Table>
                </Card.Body>
            </Card>
        </div>
    );

}

