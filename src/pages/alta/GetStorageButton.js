import React from 'react';
import { Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import { SET_ALTA_DATA, SET_STATUS_ALTA, SET_ALTA_COUNT} from '../../contexts/GlobalStateReducer'

import { getReloadIcon } from '../../utils/styling'

export default function GetStorageButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()
    const controller = new AbortController();

    async function fetchData(url) {

        try {
            const response = await fetch(url, {signal: controller.signal})
            const data = await response.json()
            my_dispatch({type: SET_ALTA_DATA, alta_data: data})
            my_dispatch({type: SET_ALTA_COUNT, alta_count: data.storage.length})
            my_dispatch({type: SET_STATUS_ALTA, status_alta: "fetched"})

        } catch (error) {
            if (error.name === 'AbortError') {
                console.log('Fetch was aborted');
                my_dispatch({type: SET_STATUS_ALTA, status_alta: "abort"})
            } else {
                my_dispatch({type: SET_STATUS_ALTA, status_alta: error.toString()})
            }

        }
    }

    const handleClick = () => {
        let url = my_state.alta_backend.url + '/altapi/get-storage'

        let results = fetchData(url)

        my_dispatch({type: SET_STATUS_ALTA, status_alta: 'unfetched'})
    }

    return <Button className="custom-btn" variant="outline-primary" onClick={() => handleClick()}>{getReloadIcon()}&nbsp;get-storage</Button>
}