import React from 'react';
import { Card, Table } from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';


export default function RightPanel(props) {
    const [ my_state ] = useGlobalReducer()

    let renderData
    try {
        if (my_state.status_alta === 'fetched') {
            let alta_data = JSON.stringify(my_state.alta_data)
            renderData = <div>
                <Card>
                    <Table>
                        <h5>ALTA data</h5>{alta_data}
                    </Table>
                </Card>
            </div>
        }
    } catch (e) {
    }

    return (
        <div className="App">
            <Card>
                <Card.Body align={"left"}>
                    {renderData}
                </Card.Body>
            </Card>
        </div>
    );

}

