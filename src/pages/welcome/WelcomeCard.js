import React from 'react';
import {Card, Table } from 'react-bootstrap'

import welcome_image from '../../assets/welcome.jpg';

export default function WelcomeCard(props) {

    return (
        <div className="App">
            <Card>
                <Card.Body>
                    <h2>Welcome to ADEX-labs</h2>
                    <Table>
                        <img src={welcome_image} alt="welcome to adex-labs" />

                    </Table>

                    <Card.Text>
                        <li>Engineering frontend to evaluate ADEX functionalty</li>

                    </Card.Text>
                </Card.Body>
            </Card>
        </div>
    );

}

