import React from 'react';
import {Container, Card, Col, Row, Table} from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';
import GetAncillaryDPSButton from "./GetAncillaryDPSButton";
import GetPrimaryButton from "./GetPrimaryDPSButton";


export default function LeftPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    let activity
    try {
        activity = my_state.selected_activity
        if (!activity) {
            return null
        }
    } catch (e) {
        console.error('no selected_activity for process_details.leftpanel')
        return null
    }

    return (
        <div className="App">
            <Card>

                <Card.Body align={"left"}>
                    <h4>Process:</h4>
                    <Table>
                        <Row><Col sm={3} md={3} lg={3}>id:</Col><Col sm={9} md={9} lg={9}>{activity.id}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>Name:</Col><Col sm={9} md={9} lg={9}><b> {activity.name}</b></Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>dataset_id:</Col><Col sm={9} md={9} lg={9}> {activity.dataset_id}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>type:</Col><Col sm={9} md={9} lg={9}> {activity.type}</Col></Row>

                        <Row><Col sm={3} md={3} lg={3}>url:</Col><Col sm={9} md={9} lg={9}>
                            <a href={activity.url}>{activity.url}</a></Col>
                        </Row>
                        <Row><Col sm={3} md={3} lg={3}>version:</Col><Col sm={9} md={9} lg={9}> {activity.version}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>ra:</Col><Col sm={9} md={9} lg={9}> {Math.round(activity.ra*1000)/1000}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>dec:</Col><Col sm={9} md={9} lg={9}> {Math.round(activity.dec*1000)/1000}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>collection:</Col><Col sm={9} md={9} lg={9}> {activity.collection}</Col></Row>

                        <Row></Row>

                        <hr></hr>
                        <Row>
                            <Col sm={4} md={4} lg={4}><GetPrimaryButton /></Col>
                            <Col ssm={4} md={4} lg={4}><GetAncillaryDPSButton /></Col>

                        </Row>

                    </Table>

                </Card.Body>
            </Card>
        </div>
    );

}

