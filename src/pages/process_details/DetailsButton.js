import React from 'react';
import { Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {
    SET_PRIMARY_DP_URL,
} from '../../contexts/GlobalStateReducer'

import { useNavigate  } from 'react-router-dom';

export default function DetailsButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()
    let navigate = useNavigate();


    const handleClick = (id) => {
        // load primary dataproduct_details for selected dataset_id
        let url = my_state.adex_backend.url + '/adex_backend/api/v1/primary_dp/?id='+id

        // this triggers a fetch
        my_dispatch({type: SET_PRIMARY_DP_URL, primary_dp_url: url})
        let new_route =  "/dataproduct/"+ id.toString()
        navigate(new_route)
    }

    return <Button className="custom-btn-short" variant="outline-primary" onClick={() => handleClick(props.id)}>{props.name}</Button>
}