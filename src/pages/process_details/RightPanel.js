import React, {useEffect} from 'react';
import {Container, Card, Col, Row, Table, Dropdown} from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';
import DetailsButton from "./DetailsButton";
import DataProductViewButton from "../skyview/DataProductViewButton";
import GotoButton from "./GotoButton";

export default function RightPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    let renderRight

    try {
        if (my_state.ancillary_data) {

            let renderAncilleryData = my_state.ancillary_data.map(dp => {
                return <div>
                    <Row>
                        <Col>{dp.collection}</Col>
                        <Col>{dp.dataset_id}</Col>
                        <Col ><a href={my_state.adex_backend.url + '/adex_backend/api/v1/ancillary_dp/?id='+dp.id} target="_blank">{dp.id}</a></Col>
                        <Col ><a href={my_state.adex_backend.url + '/adex_backend/api/v1/ancillary_dp/?dp_type='+dp.dp_type} target="_blank">{dp.dp_type}</a></Col>
                        <Col><a href={dp.access_url} target="_blank">Download</a></Col>
                    </Row>
                </div>
            })

            renderRight = <div>
                <h4>Ancillary DPS for this Process</h4>
                <Table>
                    {renderAncilleryData}
                </Table>
            </div>
        }
    } catch (e) {
    }


    try {
        if (my_state.sibling_data) {
            let renderSiblingData = my_state.sibling_data.map(dp => {
                return <div>
                    <Row>
                        <Col><a href={my_state.adex_backend.url + '/adex_backend/api/v1/primary_dp/?id='+dp.id} target="_blank">{dp.id}</a></Col>
                        <Col><DataProductViewButton id={dp.id} name="Dataproduct"/></Col>
                        <Col><GotoButton id={dp.id} ra={dp.ra} dec={dp.dec}/> </Col>
                        <Col><a href={dp.access_url}>download</a></Col>
                        <Col>{dp.name}</Col>
                        <Col>{Math.round(dp.ra*1000)/1000}</Col>
                        <Col>{Math.round(dp.dec)/1000}</Col>
                        <Col>{dp.dp_type}</Col>
                    </Row>
                </div>
            })

            renderRight = <div>
                <h4>Primary DPS for this Process</h4>
                <Table>
                    {renderSiblingData}
                </Table>
            </div>
        }
    } catch (e) {
    }


    return (
        <div className="App">
            <Card>
                <Card.Body align={"left"}>
                    {renderRight}
                </Card.Body>
            </Card>
        </div>
    );

}

