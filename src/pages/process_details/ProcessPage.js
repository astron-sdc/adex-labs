import React, {useEffect} from "react";
import { Container, Row, Col, Card, Table } from 'react-bootstrap';

import LoadingSpinner from '../../components/LoadingSpinner';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import LeftPanel from "./LeftPanel";
import RightPanel from "./RightPanel";
import {
    SET_ACTIVITY_URL,
} from "../../contexts/GlobalStateReducer";
import {useNavigate} from "react-router-dom";


export default function ProcessPage(props) {

    const [ my_state, my_dispatch] = useGlobalReducer()
    let navigate = useNavigate();

    if (props.activity_id) {

        if (my_state.status_activity === 'unfetched') {
            let url = my_state.adex_backend.url + '/adex_backend/api/v1/activity/' + props.activity_id
            console.error('(1)')
            my_dispatch({type: SET_ACTIVITY_URL, activity_url: url})
            console.error('(2)')
            let new_route =  props.activity_id
            navigate(new_route)
        }
    }


    return (
        <div>
            <Container fluid>
                <Row>
                    <Col sm={4} md={4} lg={4}>
                        <LeftPanel/>
                    </Col>
                    <Col sm={8} md={8} lg={8}>
                        <RightPanel/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}