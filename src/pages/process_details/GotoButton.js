import React from 'react';
import { Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {
    ADEX_RA,
    ADEX_DEC, SET_PRIMARY_DP_URL,
} from '../../contexts/GlobalStateReducer'

import { useNavigate  } from 'react-router-dom';
import {getMoveIcon, getReloadIcon} from '../../utils/styling'

export default function GotoButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()
    let navigate = useNavigate();


    const handleClick = (id,ra,dec) => {

        // this triggers a redraw of Aladin
        my_dispatch({type: ADEX_RA, adex_ra: ra})
        my_dispatch({type: ADEX_DEC, adex_dec: dec})

        let url = my_state.adex_backend.url + '/adex_backend/api/v1/primary_dp/?id='+id

        // this triggers a fetch
        my_dispatch({type: SET_PRIMARY_DP_URL, primary_dp_url: url})

        // this triggers a fetch
        navigate('/skyview')
    }

    return <Button className="custom-btn-short" variant="outline-primary" onClick={() => handleClick(props.id, props.ra,props.dec)}>{getMoveIcon()}&nbsp;Skyview</Button>
}