import React from 'react';
import {Container, Card, Col, Row, Table} from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';
import GetAncillaryDPSButton from "./GetAncillaryDPSButton";
import GetSiblingsButton from "./GetSiblingsButton";
import NameResolvingForm from "../skyview/NameResolvingForm";

export default function LeftPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    let primary_dp
    try {
        primary_dp = my_state.selected_primary[0]
    } catch (e) {
        return null
    }

    return (
        <div className="App">
            <Card>

                <Card.Body align={"left"}>
                    <h4>Primary Dataproduct:</h4>
                    <Table>
                        <Row><Col sm={3} md={3} lg={3}>id:</Col><Col sm={9} md={9} lg={9}>{primary_dp.id}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>Name:</Col><Col sm={9} md={9} lg={9}><b> {primary_dp.name}</b></Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>pid:</Col><Col sm={9} md={9} lg={9}> {primary_dp.pid}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>dataset_id:</Col><Col sm={9} md={9} lg={9}> {primary_dp.dataset_id}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>dp_type:</Col><Col sm={9} md={9} lg={9}> {primary_dp.dp_type}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>format:</Col><Col sm={9} md={9} lg={9}> {primary_dp.format}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>locality:</Col><Col sm={9} md={9} lg={9}> {primary_dp.locality}</Col></Row>

                        <Row><Col sm={3} md={3} lg={3}>access_url:</Col><Col sm={9} md={9} lg={9}>
                            <a href={primary_dp.access_url} target={"_blank"}>{primary_dp.access_url}</a></Col>
                        </Row>
                        <Row><Col sm={3} md={3} lg={3}>timestamp:</Col><Col sm={9} md={9} lg={9}> {primary_dp.timestamp}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>ra:</Col><Col sm={9} md={9} lg={9}> {primary_dp.ra}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>dec:</Col><Col sm={9} md={9} lg={9}> {primary_dp.dec}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>equinox:</Col><Col sm={9} md={9} lg={9}> {primary_dp.equinox}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>exposure time:</Col><Col sm={9} md={9} lg={9}> {primary_dp.exposure_time}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>central freq:</Col><Col sm={9} md={9} lg={9}> {primary_dp.central_frequency}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>freq res:</Col><Col sm={9} md={9} lg={9}> {primary_dp.frequency_resolution}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>time res:</Col><Col sm={9} md={9} lg={9}> {primary_dp.time_resolution}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>bandwidth:</Col><Col sm={9} md={9} lg={9}> {primary_dp.bandwidth}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>release date:</Col><Col sm={9} md={9} lg={9}> {primary_dp.release_date}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>provider:</Col><Col sm={9} md={9} lg={9}> {primary_dp.data_provider}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>PSF_size:</Col><Col sm={9} md={9} lg={9}> {primary_dp.PSF_size}</Col></Row>
                        <hr></hr>
                        <Row><Col sm={3} md={3} lg={3}>collection:</Col><Col sm={9} md={9} lg={9}> {primary_dp.collection}</Col></Row>
                        <Row><Col sm={3} md={3} lg={3}>process:</Col><Col sm={9} md={9} lg={9}> {primary_dp.parent}</Col></Row>

                        <Row></Row>

                        <hr></hr>
                        <Row>
                            <Col sm={3} md={3} lg={3}><GetAncillaryDPSButton /></Col>
                            <Col sm={3} md={3} lg={3}><GetSiblingsButton /></Col>
                        </Row>

                    </Table>

                </Card.Body>
            </Card>
        </div>
    );

}

