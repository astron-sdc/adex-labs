import React, {useEffect} from "react";
import { Container, Row, Col, Card, Table } from 'react-bootstrap';

import LoadingSpinner from '../../components/LoadingSpinner';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import LeftPanel from "./LeftPanel";
import RightPanel from "./RightPanel";
import {SET_PRIMARY_DP_URL, SET_SELECTED_OBJECT, SET_STATUS_PRIMARY} from "../../contexts/GlobalStateReducer";
import {useNavigate} from "react-router-dom";


export default function DataProductPage(props) {

    const [ my_state, my_dispatch] = useGlobalReducer()
    let navigate = useNavigate();

    //alert(props.object_route)
    if (props.object_id) {
        if (my_state.status_primary === 'unfetched') {
            let url = my_state.adex_backend.url + '/adex_backend/api/v1/primary_dp/?id=' + props.object_id

            my_dispatch({type: SET_PRIMARY_DP_URL, primary_dp_url: url})

            // let new_route =  props.object_id
            let new_route =  "/dataproduct/"+ props.object_id.toString()
            navigate(new_route)
        }
    }


    return (
        <div>
            <Container fluid>
                <Row>
                    <Col sm={5} md={5} lg={5}>
                        <LeftPanel/>
                    </Col>
                    <Col sm={7} md={7} lg={7}>
                        <RightPanel/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}