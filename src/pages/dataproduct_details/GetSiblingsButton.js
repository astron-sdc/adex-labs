import React from 'react';
import { Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {
    SET_ANCILLARY_DATA,
    SET_SIBLING_DATA,
} from '../../contexts/GlobalStateReducer'

import {getReloadIcon, getSiblingslIcon} from '../../utils/styling'

export default function GetSiblingsButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()
    const controller = new AbortController();

    async function fetchPrimaryDPS(url) {
        try {
            const response = await fetch(url, {signal: controller.signal})
            const data = await response.json()
            my_dispatch({type: SET_SIBLING_DATA, sibling_data: data.results})
            my_dispatch({type: SET_ANCILLARY_DATA, ancillary_data: undefined})

        } catch (error) {
            if (error.name === 'AbortError') {
                console.log('Fetch was aborted');
            } else {

            }
        }
    }

    const handleClick = () => {
        // load sibling dataproducts for selected dataset_id
        try {
            let dataset_id = my_state.selected_object.dataset_id
            let url = my_state.adex_backend.url + '/adex_backend/api/v1/primary_dp/?dataset_id=' + dataset_id

            let results = fetchPrimaryDPS(url)
        } catch (e) {
            alert('no selected object')
        }
    }

    return <Button className="custom-btn" variant="outline-primary" onClick={() => handleClick()}>{getSiblingslIcon()}&nbsp;Sibling DPS</Button>
}