import React from 'react';
import { Card, Table } from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';
import DataCards from "./DataCards";


export default function RightPanel(props) {
    const [ my_state ] = useGlobalReducer()

    let renderData
    try {
        if (my_state.status_sw === 'fetched') {
            renderData = <div>
                <Card>
                    <DataCards data={my_state.sw_data} />
                </Card>
            </div>
        }
    } catch (e) {
    }

    return (
        <div className="App">
            <Card>
                <Card.Body align={"left"}>
                    {renderData}
                </Card.Body>
            </Card>
        </div>
    );

}

