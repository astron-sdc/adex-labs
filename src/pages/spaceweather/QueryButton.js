import React from 'react';
import { Button } from 'react-bootstrap';
import {FetchData} from './FetchData'
import { getReloadIcon } from '../../utils/styling'
import {useGlobalReducer} from "../../contexts/GlobalContext";

export default function QueryButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = () => {
        let base_url = "https://spaceweather.astron.nl/api2/scintillation_preview/tasks"

        let start = my_state.sw_date + 'T' + my_state.sw_starttime
        let end = my_state.sw_date + 'T' + my_state.sw_endtime
        let my_filter = '{"$and": [{"sample_start_datetime": {"$lte": "' + end + '"}}, {"sample_end_datetime": {"$gte": "'+start+'"}}]}'
        //alert(my_filter)
        //my_filter = '{"$and": [{"sample_start_datetime": {"$lte": "2020-04-15T23:59:00"}}, {"sample_end_datetime": {"$gte": "2020-04-15T00:00:00"}}]}'

        let url = base_url + '?filter=' + my_filter

        FetchData(url, my_state, my_dispatch)
    }

    return <Button className="custom-btn" variant="outline-primary" onClick={() => handleClick()}>{getReloadIcon()}&nbsp;Query</Button>
}