import {SET_STATUS_SW, SET_SW_COUNT, SET_SW_DATA} from "../../contexts/GlobalStateReducer";
import {useGlobalReducer} from "../../contexts/GlobalContext";

export async function FetchData(url, my_state, my_dispatch) {

    try {
        const response = await fetch(url);

        if (!response.ok) {
            my_dispatch({type: SET_STATUS_SW, status_sw: response.status})
            throw new Error('Network response was not ok');
        }

        // retrieve the data as json from the response
        const data = await response.json();

        // only extract and format the data that the application uses

        const records = data.results.map(item => {

            // identify the addtional files and put them in a workable structure
            let url_to_png
            let url_to_fits

            for (let i = 0; i < item.additional_files_url.length; i++) {
                let url = item.additional_files_url[i]
                if (url.includes('.png')) {
                    url_to_png = url
                } else

                if (url.includes('.fits')) {
                    url_to_fits = url
                }
            }

            return {
                target      : item.TARGET,
                url_to_png  : url_to_png,
                url_to_fits : url_to_fits
            }
        })

        //console.log(records)

        //my_dispatch({type: SET_SW_DATA, sw_data: data.results})
        //my_dispatch({type: SET_SW_COUNT, sw_count: data.results.length})
        my_dispatch({type: SET_SW_DATA, sw_data: records})
        my_dispatch({type: SET_SW_COUNT, sw_count: records.length})
        my_dispatch({type: SET_STATUS_SW, status_sw: "fetched"})

    } catch (error) {
        console.error('Error fetching data:', error);
    }

}