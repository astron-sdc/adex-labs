import React from 'react';
import {Card, Table, Button, Container, Row, Col} from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';
import DetailsButton from "./DetailsButton";
import DownloadFitsButton from "./DownloadFitsButton";

export default function DataCard(props) {
    //console.log(props.item)

    let png = props.item.url_to_png
    //console.log(png)
    //alert(png)
    let fits = props.item.url_to_fits
    let title = props.item.target

    return (
            <Card className='text-left' >
                <Card.Header as="h1">
                    <Card.Title className="mb-1 text-muted">{title}</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                        <a href={png} target="_blank"><img src={png} height="300" alt="project"/></a>
                    </Card.Text>
                 </Card.Body>
                <Card.Footer>
                <div>
                    <a href={png} target="_blank"><Button className="custom-btn-short" variant="outline-primary" >PNG</Button></a>
                    <a href={fits} target="_blank"><Button className="custom-btn-short" variant="outline-primary" >FITS</Button></a>
                </div>
                </Card.Footer>
            </Card>

    );
}

