import React from 'react';
import { Button } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';
import {
    SET_PRIMARY_DP_URL,
} from '../../contexts/GlobalStateReducer'

import { useNavigate  } from 'react-router-dom';

export default function DetailsButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()
    let navigate = useNavigate();


    const handleClick = () => {
        alert('show ')
    }

    return <Button className="custom-btn-short" variant="outline-primary" onClick={() => handleClick()}>Details</Button>
}