import React, { useState } from 'react';
import { Form, FormControl } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';

import {SET_SW_DATE} from '../../contexts/GlobalStateReducer'

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {FetchData} from './FetchData'

// typing in the search box will execute a filter and dispatch it. The observation screen responds instantly.
export default function DateBox(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()
    const [startDate, setStartDate] = useState(new Date());

    const setSWDate = (date) => {

            let year = date.getFullYear()
            let month = date.getMonth() + 1
            let day = date.getDate()
            let sw_date = year.toString() + '-'  + ("0" + month.toString()).slice(-2) + '-' + day.toString()

            my_dispatch({type: SET_SW_DATE, sw_date: sw_date})
        }

    return (
            <DatePicker selected={my_state.sw_date} onChange={(date) => setSWDate(date)} />
        );
    };

