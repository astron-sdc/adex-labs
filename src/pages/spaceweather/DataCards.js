import React from 'react';
import {Col, Container, Row} from "react-bootstrap";

import DataCard from "./DataCard";

export default function DataCards(props) {

    //console.log(props.data)

    return (
        <div>
            <Row>
                {
                    props.data.map((item) => {

                        return (
                            <DataCard item={item}/>
                        );
                    })
                }
            </Row>

         </div>
    );
}