import React from 'react';
import { Form, FormControl } from 'react-bootstrap';
import { useGlobalReducer } from '../../contexts/GlobalContext';

import {RELOAD_SW, SET_SW_STARTTIME} from '../../contexts/GlobalStateReducer'

// typing in the search box will execute a filter and dispatch it. The observation screen responds instantly.
export default function DateBox(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    // use if you want the search to start while you hit enter
        const handleKeyPress = (event) => {

        let value = event.target.value.toUpperCase()

        if (event.charCode === 13) {
            my_dispatch({type: SET_SW_STARTTIME, sw_starttime: value})
            my_dispatch({type: RELOAD_SW, reload_sw: !my_state.reload_sw})

            // prevent the enter key to reload the whole page
            event.preventDefault()
        }

    }

    return <Form inline>
        <div>Start Time:
            <FormControl
                type="text"
                placeholder= "00:00:00"
                className="mr-sm-1"
                onKeyPress={handleKeyPress}>
            </FormControl>
        </div>
        </Form>
}
