import React from 'react';
import {Container, Card, Col, Row, Table} from 'react-bootstrap'

import { useGlobalReducer } from '../../contexts/GlobalContext';

import DateBox from "./DateBox";
import StartTimeBox from "./StartTimeBox";
import EndTimeBox from "./EndTimeBox";
import QueryButton from "./QueryButton";

export default function LeftPanel(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    return (
        <div className="App">
            <Card>

                <Card.Body align={"left"}>

                    <Table>


                        <hr></hr>
                        <Row><Col><h5>Filters:</h5></Col></Row>
                        <Row>
                            <Col>
                                <DateBox />
                                <StartTimeBox />
                                <EndTimeBox />
                                <QueryButton />
                            </Col>
                        </Row>

                    </Table>
                </Card.Body>
            </Card>
        </div>
    );

}

