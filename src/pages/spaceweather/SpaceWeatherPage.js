import React, {useEffect} from "react";
import { Container, Row, Col, Card, Table } from 'react-bootstrap';

import LoadingSpinner from '../../components/LoadingSpinner';
import { useGlobalReducer } from '../../contexts/GlobalContext';

import LeftPanel from './LeftPanel'
import RightPanel from './RightPanel'


export default function SpaceWeatherPage(props) {

    const [ my_state, my_dispatch] = useGlobalReducer()

    return (
        <div>
            <Container fluid>
                <Row>
                    <Col sm={2} md={2} lg={2}>
                        <LeftPanel/>
                    </Col>
                    <Col sm={10} md={10} lg={10}>
                        <RightPanel/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}