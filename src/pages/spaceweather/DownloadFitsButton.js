import React from 'react';
import { Button } from 'react-bootstrap';

export default function DownloadFitsButton(props) {

    return <a href={props.fits} target="_blank"><Button className="custom-btn-short" variant="outline-primary" >FITS</Button></a>

}