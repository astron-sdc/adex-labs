import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMoon, faStar, faStarAndCrescent, faWrench, faSatellite, faMeteor, faGlobe, faMap, faTag, faUsers,
    faBolt, faQuestionCircle, faImage, faCloudMeatball, faCrosshairs, faRecycle, faClock, faArrowsAlt, faBackspace}
    from '@fortawesome/free-solid-svg-icons'


export const getStarsIcon = (stars) => {
    let icon = faStar
    let color = "black"
    let size = 'grey'

    if (stars) {
        icon = faStar
        size = "md"
        color = "green"
    } else {
        icon = ["far", "faStar"]
        size = "md"
        color="grey"
    }
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getClockIcon = () => {
    let icon = faClock
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getMoveIcon = () => {
    let icon = faArrowsAlt
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getBackspaceIcon = () => {
    let icon = faBackspace
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getCatalogIcon = () => {
    let icon = faGlobe
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getMapIcon = () => {
    let icon = faMap
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getReloadIcon = () => {
    let icon = faRecycle
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getCrosshairsIcon = () => {
    let icon = faCrosshairs
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getLabelIcon = () => {
    let icon = faTag
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getSiblingslIcon = () => {
    let icon = faUsers
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}

export const getImagelIcon = () => {
    let icon = faImage
    let color = "darkblue"
    let size = 'sm'
    return <FontAwesomeIcon size={size} icon={icon} color={color}  />
}